import subprocess
import sys
import json
import time
import os

# Do not start if possibly in invalid state
if os.path.isFile(".currentlyRunning"):
    print("File '.currentlyRunning' exists! This indicates that git-sync did not terminate correctly! Check that the last_commit fields within the tracked_repositories.json file correspond to the most recent commits to those repositories. If the tracked_repositories.json file is behind the git repositories, but the corresponding commits do not exist within the subversion repository, you can delete .currentlyRunning and continue. However, if the json file is behind, and the corresponding commits have already been duplicated to svn, you need to change last_commit to align with the most recent commit that has been copied to svn. Otherwise, git-sync will generate duplicate svn commits in an attempt to bring the svn up to date with the git repositories. DO NOT manually edit tracked_repositories.json while git-sync is running. (If the repository is listed in tracked_repositories.json, but it is not on the subversion repository and no commits have been copied, its last_commit field should be 'NEW')")
    exit(1)

open(".currentlyRunning", "x")

# Signal and termination handling values
db_in_valid_state = True
is_handling_signal = False

#Url of svn repository
svn_url = "https://www.bci2000.org/svn/trunk"
svn_dir= "bci2000"

#If git-sync receives a termination signal, it must store whether or not the tracked repositories file is in a valid state
def handle_signal(sig_frame):
    if !is_handling_signal:
        is_handling_signal = true;
        if db_in_valid_state:
            os.remove(".currentlyRunning")
            exit(0)
        else:
            exit(1)

signal.signal(signal.SIGINT, handle_signal)
signal.signal(signal.SIGTERM, handle_signal)
signal.signal(signal.SIGHUP, handle_signal)


def read_tracked():
    #spin if file is locked
    #This should never take more than one cycle, as the tool that makes changes to the json file also does writes as atomically as possible, and will never hold on to a lock for any longer than is necessary
    while(True):
        if os.path.isFile("tracked_respositories.lock"):
            print("cannot read tracked repositories: file tracked_respositories is locked.", file=sys.stderr)
            sleep(1)
        else:
            break
    #lock tracked repositories
    open("tracked_repositories.lock", "x")
    reposfile = open("tracked_respositories.json", "r")
    tracked_repositories_s = resposfile.read()
    reposfile.close()
    #unlock tracked repositories
    os.remove("tracked_repositories.lock")
    return json.reads(tracked_respositories)

def write_tracked(repos):
    while(True):
        if os.path.isFile("tracked_respositories.lock"):
            print("cannot read tracked repositories: file tracked_respositories is locked.", file=sys.stderr)
            sleep(1)
        else:
            break
    tracked_repositories_s = json.dumps(repos)
    open("tracked_repositories.lock", "x")
    reposfile = open(".tracked_repositories.json.tmp", "w")
    reposfile.write(tracked_repositories_s)
    reposfile.close()
    os.replace(".tracked_repositories.json.tmp", "tracked_repositories.json")
    os.remove("tracked_repositories.lock")
)

def exec_cmd(cmd, err):
    command = subprocess.run(cmd, shell=True, capture_output=True)
    if command.returncode != 0:
        print(f"{err}, stderr output: {command.stderr}", file=sys.stderr)
    return (command.stdout, command.returncode == 0)

#kind of monadic interface for chaining shell commands
class ExecResult:
    def __init__(self):
        self.err = None
        self.val = ()
    def do(self, cmd, err, store_result=False):
        if err is not None:
            return self
        else:
            try:
                (res, err) = exec_cmd(cmd, err)
                if err:
                    self.err = True
                else:
                    if store_result:
                        self.val = self.val + res
            except Exception as e:
                self.err = e
            return self

    def __getitem__(self, key):
        return self.val[key]

    def is_err():
        return not(err is None)


message_format = r"Author:%an(%ae)\nDate:%aD\n%Title:%s\nMessage:%b"
def copy_commit(commit, dirname, repo):
    message = ExecResult().do(f"git -C {dirname} log --format=%s -n 1 {commit}", f"failed to get commit info for repo {dirname}, commit {commit}", store_result=True
            ).do(f"git -C {dirname} log --format={message_format} -n 1 {commit}", f"failed to get commit info for repo {dirname}, commit {commit}", store_result=True
            ).do(f"mkdir -p ./{svn_dir}/{repo['target_dir']}", f"failed to create target directory ./{svn_dir}/{repo['target_dir']}"
            ).do(f"cp -r {dirname}/* ./{svn_dir}/{repo['target_dir']}/", f"failed to copy files from {dirname} into directory {./{svn_dir}/{repo['target_dir']}/")
    if message.is_err():
        return False;
    message = f"{dirname}: {message[0]} \\nRepository Url: {repo['url']}\\n" + message[0]
    if ExecResult().do("svn commit -m $\"{message}\"").is_err():
        return False
    return True


def checkout_svn():
    print("checking out svn")
    svncmd = subprocess.run(f"svn checkout {svn_url} ./{svn_dir} --force --non-interactive --depth infinity --username cat svn_username --password  cat svn_password", shell=True, stdout=sys.stdout)

    if svncmd.returncode != 0:
        print(f"Could not checkout BCI2000 subversion, process returned error code {svncmd.returncode}, stderr output: {svncmd.stderr.decode()}", file=sys.stderr);
        exit(1)
    #clean up unversioned files, like if a copy commit fails
    if ExecResult().do(f"svn cleanup ./{svn_dir} --remove-unversioned", "Could not cleanup svn directory").is_err():
        exit(1)
    




#THis is where the script starts, I didnt do an if name = main which was a slight mistake

checkout_svn()


while True:
    start_time = time.time()
    # Read in the tracked repositories
    # This is done every loop to enable adding respositories without reloading the script
    

    tracked_repositories = read_tracked()

    for repo in tracked_repositories:
        dirname = repo["url"].split("/")[-1]
        # Clone repository if it does not exist
        if !os.path.isDir(dirname):
            if ExecResult.do(f"git clone {repo['url']}", f"Could not clone repository {repo['url']}").is_err():
                break
            db_in_valid_state = False
            repo["last_commit"] = "NEW"
            write_tracked(tracked_repositories)
            db_in_valid_state = True


        #switch to relevant branch and fetch
        cmds = ExecResult.do(f"git -C {dirname} switch {repo['branch_name']}", f"Could not switch to branch {repo['branch_name']} in repository {dirname}" 
                ).do(f"git -C {dirname} fetch", f"Could not fetch repository {dirname}"
                ).do(f'git -C {dirname} log --format="%H" -n 1', f"Could not get last commit of repository {dirname}", store_result=True) #get last commit
        if cmds.is_err():
            break

        last_commit = cmds[0].decode()

        #If repository is new, just copy most recent commit
        if repo["last_commit"] = "NEW":
            print(f"repo {dirname} is new, copying to svn")
            #copies latest commit to svn, writes updates to json db
            db_in_valid_state = False
            if not copy_commit(last_commit, dirname, repo):
                db_in_valid_state = True
                break
            repo["last_commit"] = last_commit
            write_tracked(tracked_repositories)
            db_in_valid_state = True
        #if there are new commits, copy them (maximum of 5, in the case of a rebase or something)
        elif repo["last_commit"] != last_commit:
            print(f"repo {dirname} has new commits, copying to svn")
            last_commits, success = exec_command(f'git log --format="%H"', capture_output=True, shell=True)
            if !success:
                break
            #get ids of the last 5 commits, or last commits after most recent, ordered from least to most recent
            commit_list = last_commits.decode().split("\n")[:-1])
            commit_index = max(commit_list.index(repo["last_commit"]), 5)
            for commit in reversed(commit_list[0:commit_index-1]):
                db_in_valid_state = False
                if not copy_commit(commit, dirname, repo):
                    db_in_valid_state = True
                    break
                repo_["last_commit"] = commit
                write_tracked(tracked_repositories)
                db_in_valid_state = True
        else:
            print(f"repo {dirname} is up to date")


    checkout_svn()
    #wait until 5 minutes have elapsed
    while True:
        if start_time - time.time() >= 600:
            break
        sleep(1)
